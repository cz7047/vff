module main

import os
import flag
import net.http
import time

const version = '0.1.3b'

struct WorkerResult {
	http.Request
	status_code int
	size        u32
	words       u32
	lines       u32
}

struct RequestConfig {
	method        http.Method
	data          string
	cookies_str   string
	user_agent    string
	read_timeout  i64
	write_timeout i64
	headers       []string
mut:
	url string
}

fn (v WorkerResult) str() string {
	mut data := ''
	if v.method == http.Method.post {
		if data.len > 0 {
			data += 'post data: '
			if v.data.len > 16 {
				data += '"${v.data[0..15]}..."'
			} else {
				data += '"$v.data"'
			}
			data += '\t'
		}
	}
	return '$v.url\t$data[status: $v.status_code, size: $v.size, words: $v.words, lines: $v.lines]'
}

fn parse_header(header string) ?[]string {
	if header.count(':') < 1 && header.len != 0 {
		return error('Invalid header $header')
	}
	return [header.all_before(':').trim(' '), header.all_after(':').trim(' ')]
}

fn permute(conf &RequestConfig, m_wordlist &map[string][]string, worker_chan chan http.Request, index int) ? {
	if keyword := m_wordlist.keys()[index] {
		unsafe {
			for value in m_wordlist[keyword] { // Need unsafe for pointer indexing
				permute(&RequestConfig{
					method: conf.method
					url: conf.url.replace(keyword, value)
					data: conf.data.replace(keyword, value)
					cookies_str: conf.cookies_str.replace(keyword, value)
					user_agent: conf.user_agent
					headers: conf.headers.map(it.replace(keyword, value))
					read_timeout: conf.read_timeout
					write_timeout: conf.write_timeout
				}, m_wordlist, worker_chan, index + 1) ?
			}
		}
	} else {
		mut request := http.Request{
			method: conf.method
			url: conf.url
			data: conf.data
			cookies: cookies_mapify(conf.cookies_str) ?
			user_agent: conf.user_agent
			read_timeout: conf.read_timeout
			write_timeout: conf.write_timeout
		}
		for header in conf.headers {
			kv := parse_header(header) ?
			request.add_custom_header(kv[0], kv[1]) ?
		}
		unsafe {
			free(conf)
		}
		worker_chan <- request
	}
}

fn cookies_mapify(v string) ?map[string]string {
	mut cookies := map[string]string{}
	for cookie in v.split(';') {
		if cookie.len == 0 { // Does it even have something
			continue
		}
		if cookie.count('=') != 1 { // Does it follow the format: key=value
			return error('Invalid cookie $cookie')
		}
		kv := cookie.split('=').map(it.trim(' '))
		if kv[0].len == 0 { // Is the key empty
			return error('Invalid cookie $cookie')
		}
		cookies[kv[0]] = kv[1]
	}
	return cookies
}

fn cookies_stringify(v map[string]string) string {
	return v.keys().map('$it=${v[it]}').join(';')
}

fn main() {
	mut app := flag.new_flag_parser(os.args)
	app.application('vff')
	app.version(version)
	app.description('V Fast Fuzzer - A safe, fast and minimalist web fuzzer written in V.')
	app.arguments_description('url wordlists...')
	app.skip_executable()
	app.limit_free_args_to_at_least(2) ?
	n_threads := app.int('', 116, 40, 'threads to use in parallel [default: 40]')

	mut conf := RequestConfig{
		method: if app.string('', 88, 'get', 'HTTP method to use').to_upper() == 'POST' {
			http.Method.post
		} else {
			http.Method.get
		}
		data: app.string('', 0x64, '', 'POST data')
		user_agent: 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0'
		cookies_str: app.string('', 0x62, '', 'Cookie data "KEY0=VALUE0; KEY1=VALUE1" for copy as curl functionality.')
		read_timeout: 10 * time.second
		write_timeout: 10 * time.second
		headers: app.string_multi('', 72, 'Header `"Name: Value"`, colon separated. Use the -H flag to specify each header.')
	}

	args := app.finalize() or {
		println(app.usage())
		return
	}

	if n_threads == 0 {
		panic('Invalid thread count supplied.')
	}

	conf.url = args[0]
	println('URL: $conf.url\nThreads: $n_threads\nWordlists:')

	mut wordlists := map[string][]string{}
	for entry in args[1..] {
		path, key := if entry.count(':') == 0 { // No keyword? Implicitly take it as `FUZZ`
			entry, 'FUZZ'
		} else {
			entry.all_before_last(':'), entry.all_after_last(':')
		}
		if key in wordlists.keys() { // Panic in case of conflicting entries
			panic('keyword `$key` already used, please specify another keyword for `$path` using a colon separator\n\tExample:\t`$path:foo`')
		}
		if true !in [conf.url, conf.data, conf.cookies_str].map(it.contains(key)) {
			panic('neither url, cookies nor post data contains keyword $key\n\tIf a colon exists in the filepath, append the default FUZZ keyword after the filepath. Like:\n\tC:\\rockyou.txt:FUZZ')
		}
		println('  $path 🡪 $key')
		wordlists[key] = os.read_lines(path) ?
	}

	if conf.data.len > 0 {
		println("Post data:\n  '$conf.data'")
	}

	worker_chan := chan http.Request{cap: n_threads}
	result_chan := chan &WorkerResult{cap: n_threads}

	p := go permute(&conf, &wordlists, worker_chan, 0)
	for _ in 0 .. n_threads {
		go worker(worker_chan, result_chan)
	}
	p.wait() ?

	mut perm := 1
	for _, v in wordlists {
		perm *= v.len
	}
	for _ in 0 .. perm {
		result := *<-result_chan
		if result.status_code in [200, 204, 301, 302, 307, 401, 403, 405] {
			println(result)
		}
	}
}

fn worker(worker_chan chan http.Request, result_chan chan &WorkerResult) {
	for {
		request := <-worker_chan
		if response := request.do() {
			result_chan <- &WorkerResult{request, response.status_code, u32(response.text.len),
				u32(response.text.count(' ')) + 1, u32(response.text.count('\n')) + 1}
		} else {
			result_chan <- &WorkerResult{request, -1, 0, 0, 0}
		}
	}
}
