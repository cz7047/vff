# V Fast Fuzzer

## Installation

### Binaries
CI/CD not set up yet.

### From source
```
git clone https://bitbucket.org/cz7047/vff
cd vff
v .
```

## Why?
VFF consumes ≈ 48.98% less memory as compared to mainstream tools like Ffuf.
Thanks to Vlang whose main backend compiles to C, VFF is very fast.
The resultant binary is also very light taking only 255KB of space.
Finally, VFF can be easily cross compiled for different operating systems.